function printNumbers() {
    let output = "";
    for (let i = 1; i <= 10; i++) 
    {
      output += i + "<br>";
    }
    document.getElementById("output").innerHTML = output;
  }
  
  printNumbers(); 
  
  
let person = {
    name: 'Bhushan Wagh',
    age: 26,
    gender: 'M',
    dob: '14/03/1996'
  };
  
  
  function addRole() {
    person.role = 'Admin';
    console.log(person);
  }
  
  
  function updateInfo() {
    person.name = 'Bhushan Patil';
    person.age = 25;
    person.gender = 'F';
    console.log(person);
  }
  
  // Delete the name property from the object 
  function deleteName() {
    delete person.name;
    console.log(person);
  }
  console.log(person);

  // Find the length of a string
  let str = 'Hello World';
console.log(str.length);
  
// Convert a string to upper case and lower case
let str3 = 'JavaScript is Awesome';
console.log(str3.toUpperCase());
console.log(str3.toLowerCase());

// Find the type of value
console.log(typeof '90');
console.log(typeof [1, 2, 3]);

// Convert string  to number 
let numStr = '90';
let num = parseInt(numStr);
console.log(num);

//convert string to array
let str4 = '1,2,3,4,5,6';
let arr = str4.split(',');
console.log(arr);

//join 2 strings using addition ,assignment operators
let str5 = 'Hello';
str5 += ' World!';
console.log(str5);

// Add, subtract, multiply, divide 2 numbers
let a = 20;
let b = 5;
console.log(a + b);
console.log(a - b);
console.log(a * b);
console.log(a / b);

function numPower(base, exponent)
 {
    let result = 1;
    for (let i = 0; i < exponent; i++)
     {
      result *= base;
    }
    return result;
  }
  
  
  console.log(numPower(4, 3)); 
  
//JS code to delete all occurrences of elements in given array

  function deleteElement(arr, elem) {
    return arr.filter(item => item !== elem);
  }
  
  const inputArr1 = [2,35,12,100,25,35,99,46];
  const elementToDelete = 35;
  const outputArr = deleteElement(inputArr, elementToDelete);
  
  console.log(outputArr); 
  
//JS code to print Even numbers in given array

  function printEven(arr) {
    for (let i = 0; i < arr.length; i++) {
      for (let j = 0; j < arr[i].length; j++) {
        if (arr[i][j] % 2 === 0) {
          console.log(arr[i][j]);
        }
      }
    }
  }
  
  const inputArr = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9]
  ];
  
  printEven(inputArr); // Output: 2 4 6 8
  